import React, { Component } from "react";
import Swal from 'sweetalert2' ;
import 'animate.css';

export default class extends Component {

  Alert = (data) =>{
    Swal.fire({

      title: "ID:" + data.id + "\n" + "Email:" + data.Email + "\n" + "Name:" + data.username + "\n" + "Age:" + data.Age,
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    })
  }
  render() {
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg bg-blue-200">
          <table class="w-full text-sm text-left text-gray-500 bg-white">
            <thead class="text-xs text-gray-700 uppercase bg-blue-200 ">
              <tr>
                <th scope="col" class="px-6 py-3">
                  ID
                </th>
                <th scope="col" class="px-6 py-3">
                  EMIAL
                </th>
                <th scope="col" class="px-6 py-3">
                  USERNAME
                </th>
                <th scope="col" class="px-6 py-3">
                  AGE
                </th>
                <th scope="col" class="px-6 py-3">
                  ACTION
                </th>
              </tr>
            </thead>
            {this.props.Data.map((para) => (
            <tbody >
                <tr class="bg-white border-b dark:bg-gray-900 dark:border-gray-700 items-center">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    {para.id}
                </th>              
                  <td align="center">{para.Email === "" ? "null" : para.Email}</td>
                  <td align="center">{para.username === "" ? "null" : para.username}</td>
                  <td align="center">{para.Age === "" ? "null" : para.Age}</td>
                  <td>
                  <button onClick={()=> this.props.onButtonChange(para.id)} class={`${para.status == "Pending" ? `bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-4`
                   : `bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-4`}`}>{para.status}</button>

                    <button  onClick={()=> this.Alert(para)} class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-4">
                      Show More
                    </button>
                    
                  </td>
                </tr>
            </tbody>
            ))}
          </table>
        </div>
      </div>
    );
  }
}
