import React, { Component } from "react";
import Table from "./Table";

export class Inputcomponent extends Component {
  constructor() {
    super();
    this.state = {
      studentname: [
        // { id: 2, stuname: "Nika", email:"nika@120gmail.com",age:"21 ",status:" Pending" },
        // { id: 3, stuname: "kaka" ,email:"kaka@234gmail.com ",age:" 22",status:" pending"},
      ],

      newusername: "",
      newemail: "",
      newage: "",
      newstatus: "Pending",
    };
  }
  // name
  changedataname = (event) => {
    this.setState({
      newusername: event.target.value,
    });
  };
  // em
  changedataemail = (event) => {
    this.setState({
      newemail: event.target.value,
    });
  };
  // age
  changedataAge = (event) => {
    this.setState({
      newage: event.target.value,
    });
  };
  
  // submit
  onSubmit = () => {
    console.log("data", this.state);
    const newObj = {
      id: this.state.studentname.length + 1,
      Email: this.state.newemail,
      username: this.state.newusername,
      Age: this.state.newage,
      status: this.state.newstatus,
    };
    console.log("New object", newObj);
    this.setState({
      studentname: [...this.state.studentname, newObj],
      newusername: "",
      newemail: "",
      newage: "",
      newstatus: "Pending",

    });
  };

  onButtonChange = (id) => {
    console.log("ID : ", id);

    this.state.studentname.map((data) => {
      if (data.id == id) {
        data.status = data.status === "Pending" ? "Done" : "Pending";
      }
    });
    this.setState({
      data: this.state.studentname,
    });

   
  };

  

  render() {
    return (
      <div className=" min-h-screen bg-[#B4E4FF] container flex flex-col items-center">
        <h1 class="text-3xl font-bold text-pink-600 ">
          Please Fill Your Information
        </h1>
        <div>
        {/* emai */}
        <label
          for="email-address-icon"
          class="block mb-2 text-sm font-medium text-gray-900 "
        >
          Your Email
        </label>
        <div class="relative">
          <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <svg
              aria-hidden="true"
              class="w-5 h-5 text-gray-500 "
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
            </svg>
          </div>

          <input
            onChange={this.changedataemail}
            type="text"
            id="email-address-icon"
            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  "
            placeholder="thary@gmail.com"
          />
        </div>
        {/* name */}
        <div class="mb-4">
          <label
            class="block text-gray-700 text-sm font-bold mb-2"
            for="username"
          >
            Username
          </label>
          <input
            onChange={this.changedataname}
            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="username"
            type="text"
            placeholder="Username"
          />
        </div>

        <div class="mb-6">
          <label
            for="base-input"
            class="block mb-2 text-sm font-medium text-gray-900 "
          >
            Age
          </label>
          <input
            onChange={this.changedataAge}
            type="text"
            id="base-input"
            class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
          />
        </div>
        {/* button */}
        <div className="container py-5 px-5 mx-0 min-w-full flex flex-col items-center">
          <button
            onClick={this.onSubmit}
            class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          >
            Register
          </button>
        </div>

        {/* Table display */}
        <Table
          Data={this.state.studentname}
          onButtonChange={this.onButtonChange}
        />
        </div>
      </div>
    );
  }
}

export default Inputcomponent;
